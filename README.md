Build Genspio Docs
==================

This project uses GitLab-CI to build the
<https://smondet.gitlab.io/genspio-doc/> website.

To build everything:

    eval $(opam env)
    export dockers=_build/default/dockers.exe
    export glacip=_build/default/glacip.exe
    ocaml please.mlt configure
    jbuilder clean
    jbuilder build $dockers
    jbuilder build $glacip

Contents:

- `glacip.ml`: program which builds the website, the demos, runs the tests in
  parallel, etc.
    - `$glacip gitlabci` generates the `.gitlab-ci.yml` file.
    - The resulting GitLabCI jobs themselves call other sub-commands.
- `dockers.ml`: generates a few `Dockerfiles` which are used to update the
  [`smondet/genspio-doc-dockerfiles`](https://github.com/smondet/genspio-doc-dockerfiles)
  repository. The docker images are build by the Docker hub:
  [`hub.docker.com/r/smondet/genspio-doc-dockerfiles/`](https://hub.docker.com/r/smondet/genspio-doc-dockerfiles/builds/) and used by the GitLabCI workflow.

<div><a href="https://user-images.githubusercontent.com/617111/47047006-784c4800-d164-11e8-9ce2-49973f406def.png"><img
  width="80%"
  style="border: solid 3px #222"
  src="https://user-images.githubusercontent.com/617111/47047006-784c4800-d164-11e8-9ce2-49973f406def.png"
></a></div>

