open Nonstd
module String = Sosa.Native_string

let ( // ) = Filename.concat

let dbg fmt = ksprintf (printf "mkdoc: %s\n%!") fmt

let cmdf fmt =
  ksprintf
    (fun s ->
      match Sys.command s with
      | 0 ->
          ()
      | other ->
          ksprintf failwith "Command %S returned %d" s other)
    fmt

let make_dockerfile ~ocaml_version () =
  let buf = Buffer.create 42 in
  let addf fmt = ksprintf (Buffer.add_string buf) fmt in
  let runf fmt = ksprintf (addf "RUN %s\n") fmt in
  let suf fmt = ksprintf (addf "RUN sudo %s\n") fmt in
  let oef fmt = ksprintf (runf "opam config exec -- %s") fmt in
  addf "FROM ocaml/opam2:ubuntu-lts\n" ;
  suf "apt-get update -y" ;
  suf "apt-get install -y pandoc wget zsh mksh busybox posh ksh" ;
  runf
    "opam repository add --rank 1 --set-default mothership \
     https://github.com/ocaml/opam-repository.git" ;
  oef "opam update --yes" ;
  oef "opam switch %s" ocaml_version ;
  oef "opam install --yes depext" ;
  List.iter
    ~f:(fun (p, u) -> oef "opam pin add -n %s '%s'" p u)
    [ ("febusy", "https://gitlab.com/smondet/febusy.git")
    ; ( "caml2html"
      , "https://github.com/smondet/caml2html.git#fix-build-407-issue-5" ) ] ;
  let packages =
    "rresult nonstd sosa uri febusy cmdliner merlin base dune.1.11.3"
  in
  (* caml2html needs `--unlock-base` so cannot be in the depext call: *)
  oef "opam depext -y  %s" packages ;
  oef "opam install --unlock-base -y caml2html %s" packages ;
  buf

let tests () =
  let buf = Buffer.create 42 in
  let addf fmt = ksprintf (Buffer.add_string buf) fmt in
  let runf fmt = ksprintf (addf "RUN %s\n") fmt in
  let suf fmt = ksprintf (addf "RUN sudo %s\n") fmt in
  let oef fmt = ksprintf (runf "opam config exec -- %s") fmt in
  let tests_to_build =
    [ ("genspio-test-main", "_build/default/src/test/main.exe")
    ; ("genspio-downloader", "_build/default/src/examples/downloader.exe")
    ; ( "genspio-small-examples"
      , "_build/default/src/examples/small_examples.exe" )
    ; ("genspio-vm-tester", "_build/default/src/examples/vm_tester.exe")
    ; ( "genspio-service-composer"
      , "_build/default/src/examples/service_composer.exe" ) ]
  in
  addf "FROM smondet/genspio-doc-dockerfiles:main406\n" ;
  runf "git clone https://github.com/hammerlab/genspio.git" ;
  addf "WORKDIR ./genspio/\n" ;
  oef "ocaml please.mlt configure" ;
  oef "dune build --profile release @install" ;
  List.iter tests_to_build ~f:(fun (n, p) ->
      oef "dune build --profile release %s" p ;
      suf "cp %s /usr/bin/%s" p n) ;
  buf

let output_and_commit_all output =
  let in_git fmt = ksprintf (cmdf "cd %s ; %s" output) fmt in
  let builds =
    [("master", make_dockerfile ~ocaml_version:"4.05")]
    @ [("apps406", tests)]
    @ List.map [3; 4; 5; 6; 7] ~f:(fun minor ->
          ( sprintf "main40%d" minor
          , make_dockerfile ~ocaml_version:(sprintf "4.0%d" minor) ))
  in
  List.iter builds ~f:(fun (name, mk) ->
      let docker_main = mk () in
      in_git "git checkout -b %s || git checkout %s" name name ;
      let o = open_out (output // "Dockerfile") in
      Buffer.output_buffer o docker_main ;
      close_out o ;
      in_git "git add Dockerfile" ;
      in_git "git commit -m 'Edit %s' || echo Ignoring" name) ;
  printf "Done, just go to %s and `git push --all`.\n%!" output

let () =
  let open Cmdliner in
  let default_cmd =
    let doc = "Build Docker images for Genspio's tests and website." in
    let sdocs = Manpage.s_common_options in
    let exits = Term.default_exits in
    ( Term.(ret (const (fun _ -> `Help (`Pager, None)) $ pure ()))
    , Term.info "genspio-dockers" ~version:"0.0.0" ~doc ~sdocs ~exits )
  in
  let build_dockerfiles_cmd =
    let open Term in
    let term =
      pure (fun output ->
          let output =
            match Filename.is_relative output with
            | true ->
                Sys.getenv "PWD" // output
            | false ->
                output
          in
          printf "Let's go → %s\n%!" output ;
          output_and_commit_all output)
      $ Arg.(
          required
          & pos 0 (some string) None
          & info [] ~doc:"The path to write stuff in.")
    in
    (term, info "build" ~doc:"Build the Dockerfiles.")
  in
  let cmds = [default_cmd; build_dockerfiles_cmd] in
  Term.(exit @@ eval_choice default_cmd cmds)
