open Nonstd
module String = Sosa.Native_string

let ( // ) = Filename.concat

let dbg fmt = ksprintf (printf "gilacipi: %s\n%!") fmt

module Gitlab_ci = struct
  module Job = struct
    type t =
      { name: string
      ; image: string
      ; artifacts: string list
      ; only: string list
      ; except: string list
      ; script: string list
      ; dependencies: string list }

    let make ?(artifacts = []) ?(only = []) ?(except = []) ?(dependencies = [])
        ~image name script =
      {name; image; artifacts; only; except; script; dependencies}
  end

  module Stage = struct
    type t = {name: string; jobs: Job.t list}

    let make name jobs = {name; jobs}
  end

  type t = {stages: Stage.t list}

  let make stages = {stages}

  module Yaml = struct
    type t =
      | File of t list
      | KV of string * string
      | V of string
      | Section of string * t list
      | Comment of string

    let file l = File l

    let section s l : t = Section (s, l)

    let v s : t = V s

    let kv s v = KV (s, v)

    let cmd t = Comment t

    let rec print ?(indent = 0) o =
      let prindent () = fprintf o "%s" (String.make indent ' ') in
      function
      | File l ->
          List.iter l ~f:(fun t -> print ~indent o t ; fprintf o "\n")
      | KV (k, v) ->
          prindent () ; fprintf o "%s: %s\n" k v
      | V k ->
          prindent () ; fprintf o "- %s\n" k
      | Section (k, vl) ->
          prindent () ;
          fprintf o "%s:\n" k ;
          List.iter vl ~f:(fun t -> print ~indent:(indent + 2) o t)
      | Comment s ->
          prindent () ; fprintf o "# %s\n" s
  end

  let to_yaml t =
    let stage_strings = List.map t.stages ~f:(fun s -> s.name) in
    let open Yaml in
    let section_if_not_empty n l f =
      match l with [] -> [] | _ -> [section n (f l)]
    in
    file
      ( [section "stages" @@ List.map stage_strings ~f:v]
      @ List.concat_map t.stages
          ~f:
            Stage.(
              fun {name; jobs} ->
                [ksprintf cmd "Stage %S" name]
                @ List.map jobs
                    ~f:
                      Job.(
                        fun j ->
                          section j.name
                          @@ [kv "image" j.image; kv "stage" name]
                          @ section_if_not_empty "artifacts" j.artifacts
                              (fun more ->
                                [ section "paths" (List.map more ~f:v)
                                ; kv "when" "always" ])
                          @ section_if_not_empty "only" j.only (fun l ->
                                List.map l ~f:v)
                          @ section_if_not_empty "except" j.except (fun l ->
                                List.map l ~f:v)
                          @ section_if_not_empty "dependencies" j.dependencies
                              (fun l -> List.map l ~f:v)
                          @ [section "script" @@ List.map j.script ~f:v])) )
end

module Names = struct
  let dockers = "_build/default/dockers.exe"

  let glacip = "_build/default/glacip.exe"

  let bins = "bin-artifacts"

  let glacip_bin = bins // Filename.basename glacip

  let gdimage = sprintf "smondet/genspio-doc-dockerfiles:main40%d"

  let genspio_source_dir = "genspio-src"

  let build_name checkout ocaml_minor =
    sprintf "build-%s-40%d" checkout ocaml_minor

  let source_path checkout ocaml_minor =
    build_name checkout ocaml_minor // genspio_source_dir

  let demo_build_name checkout ocaml_minor =
    sprintf "demo-%s-40%d" checkout ocaml_minor

  let demo_path checkout ocaml_minor =
    sprintf "demo-%s-40%d/demo" checkout ocaml_minor

  let test_assembly checkout ocaml_minor =
    sprintf "assemble-tests-%s-40%d" checkout ocaml_minor
end

module Actions = struct
  let build_genspio ~output checkout =
    let open Febusy.Edsl in
    (*   let vars =
 *     sprintf "export tmpdir='%s'\nexport output_path=/%s\nexport checkout=%s\n"
 *       tmpdir output checkout
 *   in
 *   let prelude = vars ^ {sh|
 * mkdir -p $tmpdir $output_path
 * |sh} in *)
    let tmpdir = output // "tmp" in
    let script_content =
      let oredoc_uri =
        "https://www.dropbox.com/s/aoz4vbeqeyw7bno/oredoc-amd64-bin?raw=1"
      in
      let make_script l =
        let b = Buffer.create 42 in
        let pf fmt = ksprintf (Buffer.add_string b) fmt in
        pf "set -e\n" ;
        List.iter l ~f:(fun c ->
            pf "# %S\nprintf '==== Command: %%s\\n' %s\n%s\n" c
              (Filename.quote c) c) ;
        Buffer.contents b
      in
      match checkout with
      | "genspio.0.0.0" ->
          let script =
            make_script
              [ "opam install --yes solvuu-build.0.3.0"
              ; sprintf "mkdir -p %s/_bin/" output
              ; sprintf "wget -O %s/_bin/oredoc '%s'" output oredoc_uri
              ; sprintf "chmod a+rx %s/_bin/oredoc" output
              ; sprintf "export PATH=$PWD/%s/_bin/:$PATH" output
              ; sprintf
                  "git clone https://github.com/hammerlab/genspio.git %s/%s"
                  output Names.genspio_source_dir
              ; sprintf "cd %s/%s" output Names.genspio_source_dir
              ; sprintf "git checkout %s" checkout
              ; "make"
              ; "export CSS=../master/ssc.css"
              ; "make doc"
              ; "mkdir -p ../doc/"
              ; "cp -r _build/doc/* ../doc/" ]
          in
          return (string @@ "script-content-" ^ checkout) script
      | "genspio.0.0.1" | "genspio.0.0.2" ->
          let script =
            let test_dune_name = "src/test/main.exe" in
            let test_exe = "_build/default/" // test_dune_name in
            make_script
              [ sprintf "export genspio_demo_url=../demo/%s/index.html\n"
                  checkout
              ; "opam install --yes jbuilder odoc"
              ; sprintf "mkdir -p %s/" output
              ; sprintf
                  "git clone https://github.com/hammerlab/genspio.git %s/%s"
                  output Names.genspio_source_dir
              ; sprintf "cd %s/%s" output Names.genspio_source_dir
              ; sprintf "git checkout %s" checkout
              ; sprintf "ocaml please.mlt configure"
              ; sprintf "echo '(lang dune 1.0)' > dune-project"
              ; sprintf "jbuilder build %s" test_dune_name
              ; "sh tools/build-doc.sh"
              ; "mkdir -p ../doc/"
              ; "cp -r _build/doc/html/* ../doc"
              ; sprintf "rm -fr ../test/"
              ; sprintf "%s --important-shells bash,dash ../test" test_exe ]
          in
          return (string @@ "script-content-" ^ checkout) script
      | "master" | _ ->
          let script =
            let test_dune_name = "src/test/main.exe" in
            let test_exe = "_build/default/" // test_dune_name in
            make_script
              [ sprintf "export genspio_demo_url=../demo/%s/index.html\n"
                  checkout
              ; "opam install --yes dune.1.11.3 fmt base odoc"
              ; sprintf "mkdir -p %s/" output
              ; sprintf
                  "git clone https://github.com/hammerlab/genspio.git %s/%s"
                  output Names.genspio_source_dir
              ; sprintf "cd %s/%s" output Names.genspio_source_dir
              ; sprintf "git checkout %s" checkout
              ; sprintf "dune build %s" test_dune_name
              ; "sh tools/build-doc.sh"
              ; "mkdir -p ../doc/"
              ; "ls -l _build/doc/html/*"
              ; "cp -r _build/doc/html/* ../doc"
              ; sprintf "rm -fr ../test/"
              ; sprintf "%s --important-shells bash,dash ../test" test_exe ]
          in
          return (string @@ "script-content-" ^ checkout) script
    in
    script_content
    >>= fun _ script ->
    File.make
      ( tmpdir
      // sprintf "script-%s-%s"
           (Digest.string output |> Digest.to_hex)
           checkout )
      (fun script_path ->
        System.cmdf "mkdir -p %s %s" tmpdir output ;
        System.write_lines script_path [script])
    >>= fun _ (`File script) ->
    let logfile = tmpdir // sprintf "build-api-%s" checkout in
    File.make
      (output // "doc" // "index.html")
      (fun _ ->
        dbg "Running %s -> %s" script logfile ;
        System.cmdf "bash %s" (* " >> %s 2>&1" *) script (* logfile logfile *))

  let build_webdemo ~output ~tmpdir checkout =
    let open Febusy.Edsl in
    let repodir = tmpdir // "genspio-web-demo" in
    let repo =
      File.make (repodir // ".git/config") (fun _ ->
          System.cmdf
            "cd %s ; git clone -b master  \
             https://gitlab.com/smondet/genspio-web-demo.git"
            tmpdir)
    in
    let deps =
      phony (sprintf "demo-deps-%s" checkout)
      <-- fun () ->
      let logfile = tmpdir // "demo-deps.log" in
      dbg "Getting DEMO deps (%s)" checkout ;
      System.cmdf
        "opam install --yes fmt base sosa nonstd rresult dune.1.11.3 \
         js_of_ocaml-tyxml.3.0.1 js_of_ocaml-toplevel.3.0.1 \
         js_of_ocaml-lwt.3.0.1 >> %s 2>&1"
        logfile ;
      System.cmdf
        "opam pin add --yes -k git genspio \
         'https://github.com/hammerlab/genspio.git#%s' >> %s 2>&1"
        checkout logfile
    in
    repo =<>= deps
    >>= fun _ (`File _, _) ->
    File.make
      (Sys.getcwd () // output // "demo" // checkout // "index.html")
      (fun p ->
        let logfile = tmpdir // sprintf "demo-build-%s.log" checkout in
        dbg "Building DEMO for %s" checkout ;
        let cmds =
          [ sprintf "cd %s" repodir
          ; "dune --version"
          ; "opam list | grep js_of"
          ; "set -e"
          ; sprintf "export genspio_version_name=%s" checkout
          ; "eval `opam env`"
          ; "sh build.sh"
          ; sprintf "mkdir -p %s" (Filename.dirname p)
          ; sprintf "cp -r _build/custom/* %s" (Filename.dirname p)
          ; sprintf "echo 'Done-demo-build %s'" checkout ]
        in
        System.cmdf " ( %s ; ) ; if [ $? -ne 0 ] ; then cat %s ; exit 2 ; fi"
          (String.concat ~sep:" && " cmds)
          logfile)

  let list_subdirectories dir =
    let rr = Sys.readdir dir in
    Array.sort ~cmp:String.compare rr ;
    Array.to_list rr |> List.filter ~f:(fun v -> dir // v |> Sys.is_directory)

  let run_tests ~test_dir range =
    let tests_to_run =
      let all = list_subdirectories test_dir in
      (* Array.sort String.compare rr ;
         * Array.to_list rr
         * |> List.filter ~f:(fun v -> test_dir // v |> Sys.is_directory) *)
      (* in *)
      List.take (List.drop all (fst range)) (snd range)
    in
    let open Febusy.Edsl in
    join
      (List.map tests_to_run ~f:(fun t ->
           File.make
             (test_dir // t // "report.md")
             (fun _ ->
               System.cmdf "cd %s/%s ; make clean ; make ; make report"
                 test_dir t)))

  let pandoc ~i ~o ~title ~css =
    let open Febusy.Edsl in
    System.cmdf
      "mkdir -p %s ; pandoc -i %s -o %s -s --toc -V pagetitle='%s' -V \
       title='%s' --css %s"
      (Filename.dirname o) i o title title css

  let test_page ~test_dir ~with_successes ~output_dir =
    let open Febusy.Edsl in
    let test_dirs = list_subdirectories test_dir in
    let css = "../../master/ssc.css" in
    System.cmdf "mkdir -p %s %s" test_dir output_dir ;
    let test_info =
      List.map test_dirs ~f:(fun p ->
          let listsubdir s =
            try Sys.readdir (sprintf "%s/%s/%s/" test_dir p s) |> Array.to_list
            with _ -> []
          in
          let failures = listsubdir "_failure" in
          let successes = listsubdir "_success" in
          let sn = List.length successes in
          let fn = List.length failures in
          let sty = if fn > 0 then "color: #500" else "color: #050" in
          let details =
            let links_section result_files =
              List.map result_files ~f:(fun flure ->
                  let nn = Filename.chop_extension flure in
                  let logpath = sprintf "%s/%s/_log/%s/" test_dir p nn in
                  let script s = sprintf "%s/%s/script/%s%s" test_dir p nn s in
                  let files =
                    File.List.return
                      [ logpath // "stderr.txt"
                      ; logpath // "stdout.txt"
                      ; script "-script.sh"
                      ; script "-run-test.sh"
                      ; script "-display.scm" ]
                    >>= fun _ files ->
                    File.List.make
                      (List.map files ~f:(fun (`File f) ->
                           output_dir
                           // sprintf "test-%s-%s-%s" p flure
                                (Filename.basename f)))
                      (fun l ->
                        List.map2 files l ~f:(fun (`File f) o ->
                            System.cmdf ~silent:true "cp %s %s" f o))
                  in
                  files
                  >>= fun _ ofiles ->
                  ksprintf string "item-%s-%s-%s" test_dir p flure
                  <-- fun () ->
                  sprintf "* **%s:**</br>%s" nn
                    ( List.map ofiles ~f:(fun (`File f) ->
                          let c =
                            match Filename.basename f with
                            | b when String.is_suffix b ~suffix:"out.txt" ->
                                "`stdout`"
                            | b when String.is_suffix b ~suffix:"err.txt" ->
                                "`stderr`"
                            | b when String.is_suffix b ~suffix:"script.sh" ->
                                "compiled script"
                            | b when String.is_suffix b ~suffix:"scm" ->
                                "pretty-printed lisp"
                            | b when String.is_suffix b ~suffix:"test.sh" ->
                                "test-runner"
                            | other ->
                                other
                          in
                          sprintf "[`%s`](./%s)" c (Filename.basename f))
                    |> String.concat ~sep:", " ))
              |> join
            in
            links_section failures
            =<>= (if with_successes then links_section successes else join [])
            >>= fun _ (failures, successes) ->
            let name = test_dir // sprintf "test-%s-info" p in
            string ("content-" ^ name)
            <-- (fun () ->
                  ( [sprintf "### Test %s" p; ""]
                  @ ( match failures with
                    | [] ->
                        ["#### No Failures"; ""]
                    | more ->
                        ["#### Failures"; ""] @ more )
                  @ [""; ""]
                  @
                  match successes with
                  | _ when not with_successes ->
                      [ "#### Successes' Information Was Disabled For This \
                         Branch"
                      ; "" ]
                  | [] ->
                      ["#### No Successes"; ""]
                  | more ->
                      ["#### Successes"; ""] @ more )
                  |> String.concat ~sep:"\n")
            >>= fun _ content ->
            File.make
              (output_dir // sprintf "test-%s-info.html" p)
              (fun fp ->
                let i = test_dir // sprintf "test-%s-info.md" p in
                System.write_lines i [content] ;
                pandoc ~o:fp ~i ~css ~title:(sprintf "Test %s" p))
          in
          let item =
            details
            >>= fun _ (`File details) ->
            string (sprintf "Test item %s %s" test_dir p)
            <-- fun () ->
            sprintf "**`%s:`** <span style=%S>[%d/%d]</span> [details](./%s)" p
              sty sn
              (sn + List.length failures)
              (Filename.basename details)
          in
          item)
      |> join
    in
    let sys_info =
      let commands =
        [ "uname -a"
        ; "ocaml -version"
        ; "opam --version"
        ; "getconf -a"
        ; "ulimit -a"
        ; "{ xargs --show-limits 2>&1 ; } & { sleep 1 ; exit 0 ; } " ]
        @ List.map ~f:(sprintf "dpkg -s %s")
            ["dash"; "bash"; "mksh"; "ksh"; "posh"; "busybox"; "zsh"]
      in
      join
      @@ List.map commands ~f:(fun c ->
             ksprintf string "result-of-%s" c
             <-- fun () ->
             let sty =
               "padding: 4px;margin: 5px ;border: solid 2px #aaa ;color: \
                #ddd;background-color: #111;"
             in
             sprintf
               "<details style=%S><summary><code><span style=\"color: #fff; \
                font-weight: bold\"> 🢧 </span>$ %s</code></summary> \
                <code><pre>%s</pre></code></details>"
               sty c
               (System.cmd_to_string_list c |> String.concat ~sep:"\n"))
    in
    test_info =<>= sys_info
    >>= fun _ (test_info_list, sysinfo) ->
    string (sprintf "index-content-tests-%s" test_dir)
    <-- (fun () ->
          [ ""
          ; "Built on: "
          ; System.cmd_to_string_list "date -R" |> List.hd_exn
          ; "" ]
          @ [""; "### Results"; ""]
          @ List.map test_info_list ~f:(fun d -> sprintf "* %s" d)
          @ [""; "### System Information"; ""]
          @ List.map sysinfo ~f:(sprintf "%s")
          @ [""; ""]
          |> String.concat ~sep:"\n")
    >>= fun _ content_md ->
    File.make (test_dir // "index.md") (fun p ->
        System.write_lines p [content_md])
    >>= fun _ (`File index_md) ->
    File.make (output_dir // "index.html") (fun p ->
        pandoc ~i:index_md ~o:p ~title:(sprintf "Tests %S" test_dir) ~css)

  let index_md ~checkouts ~test_checkouts ~tmpdir:_ ~ocaml_minor () =
    let image ~width uri =
      sprintf
        "<div style=\"padding: 2em; margin: 2em;\"><a href=%S><img src=%S \
         width=%S></a></div>"
        uri uri width
    in
    let section title l = [""; "### " ^ title; ""] @ l in
    let par l = l @ [""; ""] in
    section "Documentation"
      ( par
          [ "Welcome to the documentation for the Genspio library, various \
             tags/branches are available:" ]
      @ par
          (List.map checkouts ~f:(fun chkt ->
               let src_path =
                 Names.source_path chkt
                   (if chkt = "genspio.0.0.0" then 3 else ocaml_minor)
               in
               let hash =
                 Febusy.Edsl.System.cmd_to_string_list
                   (sprintf "cd %s && git rev-parse HEAD" src_path)
                 |> List.hd_exn
               in
               let subj, date, relative_time =
                 match
                   Febusy.Edsl.System.cmd_to_string_list
                     (sprintf
                        "cd %s && git log --format='%%s%%n%%cD'  HEAD^..HEAD"
                        src_path)
                 with
                 | a :: b :: _ ->
                     (a, b, String.sub_exn b ~index:0 ~length:16)
                 | other ->
                     ksprintf failwith "Can't get last log: %s"
                       (String.concat ~sep:", " other)
               in
               let hash8 = String.take hash ~index:8 in
               let chkt_style = "font-weight: bold ; color: #600" in
               let kind, name =
                 match String.chop_prefix chkt ~prefix:"genspio." with
                 | Some v ->
                     ("Version", v)
                 | None ->
                     ("Branch", chkt)
               in
               sprintf
                 "* <span style=%S>%s <code>%s</code>:</span> \n\
                 \    - [Documentation](./%s/index.html), \n\
                 \    - Interactive [top-level/demo](./demo/%s/index.html),\n\
                 \    - Source at commit \
                  [`%s`](https://github.com/hammerlab/genspio/tree/%s) \
                  (*“%s”* — <abbr title=%S>%s</abbr>)."
                 chkt_style kind name chkt chkt hash8 hash subj date
                 relative_time))
      @ par
          [ "See also the source on Github: "
          ; "[`hammerlab/genspio`](https://github.com/hammerlab/genspio)." ] )
    @ section "Presentations"
        (let seb_author = "*[Seb Mondet](http://seb.mondet.org)*" in
         par
           [ "Genspio was presented at least twice: "
           ; ""
           ; "- At [OCaml \
              2017](https://github.com/gasche/icfp2017-papers/blob/master/README.md#ocaml-workshop-2017):"
           ; "**Genspio: Generating Shell Phrases In OCaml**,"
           ; seb_author
           ; "([extended \
              abstract](https://wr.mondet.org/paper/smondet-genspio-ocaml17.pdf),"
           ; "[slides](https://wr.mondet.org/slides/OCaml2017-Genspio/))."
           ; "- At the [Compose conference \
              2019](http://www.composeconference.org/2019/): "
           ; "**Genspio: An EDSL to Generate POSIX Shell Garbage**,"
           ; seb_author
           ; "([slides](https://wr.mondet.org/slides/Genspio-Compose2019/index.html),"
           ; "Youtube \
              [video](https://www.youtube.com/watch?v=daCkNUf85uA&list=PLNoHgLVTxtaorTczyo8NA3tg_vK8WC5rD&index=4))."
           ])
    @ section "About The Demo / Interactive Toplevel"
        ( par
            [ "The demo-playgrounds above are implemented separately in a \
               GitLab repository: \
               [`smondet/genspio-web-demo`](https://gitlab.com/smondet/genspio-web-demo/); \
               they use [`Tyxml_js`](https://github.com/ocsigen/tyxml), \
               [`react`](http://erratique.ch/software/react), and an OCaml \
               toplevel running within a WebWorker. Feel free to report \
               issues or send “pull/merge requests.”" ]
        @ par
            [ image ~width:"95%"
                "https://user-images.githubusercontent.com/617111/33030073-56815ae2-cde8-11e7-8406-c3986e682973.gif"
            ] )
    @ section "About This Website"
        ( par
            [ "This website is developed at \
               [`gitlab.com/smondet/genspio-doc/`](https://gitlab.com/smondet/genspio-doc/), \
               it is a nice example of usage of the \
               [Febusy](https://smondet.gitlab.io/febusy) library." ]
        (* match with_tests with
       * | false -> []
       * | true -> *)
        @ par ["It also happens to run all the Genspio tests, *again*:"]
        @ List.map test_checkouts ~f:(function
            | "genspio.0.0.0" as chkt ->
                sprintf "* Not supported for `%s`." chkt
            | chkt ->
                sprintf "* `%s` → [results](tests/%s/index.html)" chkt chkt)
        )

  let index_page ~tmpdir ~checkouts ~test_checkouts ~output ~ocaml_minor =
    let open Febusy.Edsl in
    System.cmdf "mkdir -p %s %s" tmpdir output ;
    List.iter checkouts ~f:(fun chkt ->
        let ocaml = if chkt = "genspio.0.0.0" then 3 else ocaml_minor in
        let doc_path = Names.build_name chkt ocaml // "doc" in
        let demo_path = Names.demo_path chkt ocaml in
        System.cmdf "cp -r %s %s/%s" doc_path output chkt ;
        System.cmdf "mkdir -p %s/demo" output ;
        System.cmdf "cp -r %s/* %s/demo/" demo_path output) ;
    List.iter test_checkouts ~f:(fun chkt ->
        let test_assembly = Names.test_assembly chkt ocaml_minor in
        System.cmdf "mkdir -p %s/tests" output ;
        System.cmdf "cp -r %s %s/tests/%s" test_assembly output chkt) ;
    string "index-md-content"
    <-- (fun () ->
          index_md ~checkouts ~test_checkouts ~tmpdir ~ocaml_minor ()
          |> String.concat ~sep:"\n")
    >>= fun _ index_md_content ->
    File.make (tmpdir // "index.md") (fun p ->
        System.write_lines p [index_md_content])
    >>= fun _ (`File mdindex) ->
    File.make (output // "index.html") (fun fphtml ->
        let css = "master/ssc.css" in
        let title = "Genspio Documentation" in
        System.cmdf "find %s -type l -exec rm {} \\;" output ;
        System.cmdf
          "mkdir -p %s ; pandoc -i %s -o %s -s --toc -V pagetitle='%s' -V \
           title='%s' --css %s"
          output mdindex fphtml title title css)
end

module Gitlab_ci_configuration = struct
  let build_bins_name = "build-bins"

  open Names

  let gitlab_build_this =
    Gitlab_ci.Job.make build_bins_name ~image:(gdimage 6) ~artifacts:[bins]
      ( [sprintf "mkdir -p %s" bins]
      @ List.concat_map [dockers; glacip] ~f:(fun bin ->
            [sprintf "dune build %s" bin; sprintf "cp %s %s" bin bins]) )

  let clean_up_genspio_doc_from_image =
    sprintf "rm -rf dune-project dune _build/"

  let gitlab_build_genspio checkout ocaml_minor =
    let name = build_name checkout ocaml_minor in
    let dir = "./" ^ name in
    (* let tmpdir = sprintf "./tmp-%s" name in *)
    let script =
      [ clean_up_genspio_doc_from_image
      ; sprintf "%s build-genspio %s %s" glacip_bin checkout dir ]
    in
    Gitlab_ci.Job.make name ~image:(gdimage ocaml_minor) ~artifacts:[dir]
      script

  let gitlab_build_demo checkout ocaml_minor =
    let name = demo_build_name checkout ocaml_minor in
    let dir = "./" ^ name in
    (* let tmpdir = sprintf "/tmp/tmp-%s" name in *)
    let script =
      [ clean_up_genspio_doc_from_image
      ; sprintf "%s build-webdemo %s %s" glacip_bin checkout dir ]
    in
    Gitlab_ci.Job.make name ~image:(gdimage ocaml_minor) ~artifacts:[dir]
      script

  let test_name checkout ocaml_minor range =
    sprintf "T%d-%d-%s-40%d" (fst range) (snd range) checkout ocaml_minor

  let gitlab_run_tests checkout ocaml_minor range =
    let name = test_name checkout ocaml_minor range in
    let dir = "./" ^ build_name checkout ocaml_minor in
    let testdir = dir // "test" in
    let script =
      [ sprintf "%s run-tests %s %d %d" glacip_bin testdir (fst range)
          (snd range) ]
    in
    Gitlab_ci.Job.make name ~image:(gdimage ocaml_minor) ~artifacts:[dir]
      ~dependencies:[build_bins_name; build_name checkout ocaml_minor]
      script

  let gitlab_assemble_tests checkout ocaml_minor test_ranges =
    let dependencies =
      build_bins_name
      :: List.map test_ranges ~f:(fun rg -> test_name checkout ocaml_minor rg)
    in
    let dir = "./" ^ build_name checkout ocaml_minor in
    let testdir = dir // "test" in
    let name = test_assembly checkout ocaml_minor in
    let output = "." // name in
    let script =
      [sprintf "%s assemble-tests %s %s %s" glacip_bin testdir "false" output]
    in
    Gitlab_ci.Job.make name ~image:(gdimage ocaml_minor)
      ~artifacts:[dir; output] ~dependencies
      (* [sprintf "cd %s/test" dir; "make report"] *)
      script

  let assemble_website ~show_checkouts ~test_checkouts ~ocamls ~test_ocamls ()
      =
    let open Gitlab_ci in
    let make_website name output =
      let only = match name with "pages" -> ["master"] | _ -> [] in
      let except = match name with "pages" -> [] | _ -> ["master"] in
      let dependencies =
        build_bins_name
        :: List.concat_map show_checkouts ~f:(function
             | "genspio.0.0.0" as c ->
                 [build_name c 3; demo_build_name c 3]
             | chkt ->
                 List.concat_map ocamls ~f:(fun oc ->
                     [build_name chkt oc; demo_build_name chkt oc]))
        @ List.concat_map test_checkouts ~f:(fun chkt ->
              List.map test_ocamls ~f:(fun oc -> test_assembly chkt oc))
      in
      Job.make name ~image:(gdimage 6) ~artifacts:[output] ~only ~dependencies
        ~except
        [ "du -h --max-depth 2"
        ; sprintf "%s assemble-website %s" glacip_bin output
        ; "du -h --max-depth 2" ]
    in
    [make_website "test-pages" "pages-test"; make_website "pages" "public"]

  let extra_branches = []

  module Full_blown_constants = struct
    let versions = ["genspio.0.0.2"; "genspio.0.0.1"]

    let branches = ["master"] @ extra_branches

    let checkouts = versions @ branches

    let show_checkouts = versions @ ["genspio.0.0.0"] @ branches

    let ocamls = [6; 7]

    let test_checkouts =
      ["genspio.0.0.2"; "genspio.0.0.1"; "master"] @ extra_branches

    let test_ocamls = [6]

    let with_000 = true

    (* There are 40 directories in each test directory. *)
    let test_ranges = List.init 10 ~f:(fun ith -> (ith * 4, 4))
  end

  module Light_testing_constants = struct
    let base = ["genspio.0.0.2"; "master"]

    let checkouts = base @ extra_branches

    let with_000 = false

    let show_checkouts =
      (if with_000 then ["genspio.0.0.0"] else []) @ checkouts

    let ocamls = [(* 3; *) 7]

    let test_checkouts = base @ extra_branches

    let test_ocamls = [7]

    let test_ranges = List.init 2 ~f:(fun ith -> (ith * 2, 2))
  end

  module Constants = Full_blown_constants

  (* module Constants = Light_testing_constants *)

  let file =
    let open Gitlab_ci in
    let open Constants in
    make
      [ Stage.make "build-builder" [gitlab_build_this]
      ; Stage.make "build-docs-demos-tests"
          ( ( if Constants.with_000 then
              [ gitlab_build_genspio "genspio.0.0.0" 3
              ; gitlab_build_demo "genspio.0.0.0" 3 ]
            else [] )
          @ List.concat_map checkouts ~f:(fun checkout ->
                List.concat_map ocamls ~f:(fun ocaml_minor ->
                    [ gitlab_build_demo checkout ocaml_minor
                    ; gitlab_build_genspio checkout ocaml_minor ])) )
      ; Stage.make "run-tests"
        @@ List.concat_map test_checkouts ~f:(fun checkout ->
               List.concat_map test_ocamls ~f:(fun ocaml_minor ->
                   List.map test_ranges
                     ~f:(gitlab_run_tests checkout ocaml_minor)))
      ; Stage.make "assemble-tests"
        @@ List.concat_map test_checkouts ~f:(fun checkout ->
               List.concat_map test_ocamls ~f:(fun ocaml_minor ->
                   [gitlab_assemble_tests checkout ocaml_minor test_ranges]))
      ; Stage.make "make-website"
          (assemble_website ~show_checkouts ~test_checkouts ~ocamls
             ~test_ocamls ()) ]
end

let make_and_exit ~on_value ~name thing =
  let open Febusy.Edsl in
  let say fmt = eprintf (fmt ^^ "\n%!") in
  let state_file = sprintf "/tmp/%s.state" name in
  let res, log = Make_unix.run ~state_file thing in
  let ret =
    match res with
    | Ok {value; _} ->
        on_value value ; 0
    | Error e ->
        say "Bulid ERROR: %s" @@ Febusy.Common.Error.to_string e ;
        1
  in
  let logfile = sprintf "/tmp/%s.log" name in
  System.write_lines logfile [log] ;
  say "Build-Log: %s" logfile ;
  exit ret

let () =
  match Sys.argv |> Array.to_list |> List.tl_exn with
  | ["gitlabci"] ->
      let o = open_out ".gitlab-ci.yml" in
      (* let fmt = Format.formatter_of_out_channel o in *)
      fprintf o "# Generated GitLabCI file\n" ;
      Gitlab_ci.Yaml.print o (Gitlab_ci.to_yaml Gitlab_ci_configuration.file) ;
      (* .fprintf fmt "# Generated GitLabCI file\n%a" Gitlab_ci.Yaml.pp
       *   (Gitlab_ci.to_yaml gitlab_ci) ; *)
      close_out o
  | ["build-genspio"; checkout; output] ->
      make_and_exit
        ~on_value:(fun _ -> ())
        ~name:"build-genspio"
        (fun () -> Actions.build_genspio checkout ~output)
  | ["build-webdemo"; checkout; output] ->
      make_and_exit
        ~on_value:(fun _ -> ())
        ~name:"build-webdemo"
        (fun () -> Actions.build_webdemo checkout ~output ~tmpdir:"/tmp/")
  | ["run-tests"; testdir; ra; rb] ->
      let i s =
        Int.of_string s |> Option.value_exn ~msg:(sprintf "int.of_string %S" s)
      in
      let range = (i ra, i rb) in
      make_and_exit
        ~on_value:(fun _ -> ())
        ~name:"run-tests"
        (fun () -> Actions.run_tests ~test_dir:testdir range)
  | ["assemble-tests"; test_dir; with_successes; output_dir] ->
      let with_successes = bool_of_string with_successes in
      make_and_exit
        ~on_value:(fun _ -> ())
        ~name:"assemble-tests"
        (fun () -> Actions.test_page ~test_dir ~with_successes ~output_dir)
  | ["assemble-website"; output] ->
      make_and_exit
        ~on_value:(fun _ -> ())
        ~name:"assemble-website"
        (fun () ->
          let open Gitlab_ci_configuration.Constants in
          Actions.index_page ~tmpdir:"/tmp/index-tmp" ~checkouts:show_checkouts
            ~test_checkouts ~output
            ~ocaml_minor:
              ( List.last test_ocamls
              |> Option.value_exn ~msg:"Not enough ocaml versions" ))
  | _ ->
      ksprintf failwith "usage: %s <command>" Sys.argv.(0)
